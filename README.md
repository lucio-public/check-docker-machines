# Check Docker Machines

This repository provides a bash script which checks if available docker machines have access to the gitlab docker registry and other services.

## Concept

This script assumes you have a working docker-machine installation and running machines. When running the script it will get a list of available machines with installed docker and run several commands via `docker-machine ssh`. It will curl some endpoints to check the access to these endpoints. If a machine does not have access it will get removed via `docker-machine rm`. Before removing the machine it will get checked by the script for some running binaries like php or mysql. Check the script for a list of binary names. It does this so it will not remove machines which currently are used in a running gitlab ci job.


## Usage

Run `bash check_machines.sh` after downloading the script. The script will create a new file `checked_machines.txt` and store already successfully checked machines.

It is recommend to run this script in a crontab and log the output to a file. 

Example:
```
*/2 * * * * bash /path/to/check_machines.sh >> /path/to/check_machines.log
0 3 * * * rm /path/to/check_machines.log
```




#!/bin/bash

# File to store the names of machines that have been successfully checked
checked_file="checked_machines.txt"

# Get the list of machine names where Docker is not "Unknown" and exclude the header
machines=$(docker-machine ls | awk 'NR>1 && $6 != "Unknown" { print $1 }')

# List of URLs to check accessibility
urls=("https://registry.gitlab.com" "https://gitlab.com/cdn-cgi/trace")

# List of process names to check for machine in use
process_names=("php" "mysqld" "chromium" "apache2" "npm" "yarn" "behat" "scp")

# Timeout for each check (in seconds)
check_timeout=30

# Function to check if a machine has already been checked successfully
is_machine_checked() {
  local machine_name="$1"
  grep -q "$machine_name" "$checked_file"
  return $?
}

# Function to mark a machine as checked successfully
mark_machine_as_checked() {
  local machine_name="$1"
  echo "$machine_name" >> "$checked_file"
}

# Function to remove a machine
remove_machine() {
  local machine="$1"
  echo "Removing Machine $machine ..."
  docker-machine rm -f "$machine"
}

# Function to check if a machine is running any specified processes
is_machine_in_use() {
  local machine="$1"
  local in_use=false

  # Check if any of the specified process names are running on the machine
  for process_name in "${process_names[@]}"; do
    if docker-machine ssh "$machine" "ps ax | grep -v grep | grep -q $process_name" >/dev/null 2>&1; then
      in_use=true
      echo "Machine $machine is currently in use by process: $process_name"
      # No need to check further if any of the specified processes are found
      break
    fi
  done

  $in_use
}
# Function to check URL accessibility for each machine
check_accessibility() {
  local machine="$1"
  local failed_urls=()

  # Check if the machine has already been successfully checked
  if is_machine_checked "$machine"; then
#    echo "Machine $machine has already been checked successfully."
    return
  fi

  echo "Checking accessibility for Machine $machine ..."

  for url in "${urls[@]}"; do
    # Run the check command with a timeout of 30 seconds and capture the output
    local response_code
    if response_code=$(docker-machine ssh "$machine" "timeout $check_timeout curl -s -o /dev/null -w '%{http_code}' $url" 2>/dev/null); then
      echo "Machine $machine can access $url (HTTP status code: $response_code)"
    else
      local exit_status=$?
      if [ $exit_status -eq 124 ]; then
        echo "Machine $machine: The 'curl' command timed out after $check_timeout seconds for $url"
      else
        echo "Machine $machine: The 'curl' command encountered an error (Exit status: $exit_status) for $url"
      fi
      failed_urls+=("$url")
    fi
  done

  if [ ${#failed_urls[@]} -gt 0 ]; then
    for url in "${failed_urls[@]}"; do
      echo "Machine $machine cannot access $url"
    done

    # Check if the machine is currently in use (has running specified processes)
    if is_machine_in_use "$machine"; then
      echo "Machine $machine is currently in use. Skipping removal."
    else
      remove_machine "$machine"
    fi
  else
    # Mark the machine as checked successfully
    mark_machine_as_checked "$machine"
  fi
}

# Iterate through the list of machines where Docker is not "Unknown"
for machine in $machines; do
  check_accessibility "$machine"
done
